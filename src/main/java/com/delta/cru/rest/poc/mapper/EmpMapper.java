/**
 * 
 */
package com.delta.cru.rest.poc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.delta.cru.rest.poc.vo.EmpVo;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
public interface EmpMapper {
	
	/**
	 * Find all emps.
	 *
	 * @return the list
	 */
	@Select("SELECT * from employee")
	@Results({ @Result(column = "EMPID", property = "emplId"), @Result(column = "LASTNAME", property = "lname"),
			@Result(column = "FIRSTNAME", property = "fname"), @Result(column = "ADDRESS", property = "address"),
			@Result(column = "CITY", property = "city") })

	List<EmpVo> findAllEmps();

	/**
	 * Find emp by id.
	 *
	 * @param emplId the empl id
	 * @return the emp vo
	 */
	@Select("SELECT * from employee where EMPID=#{emplId}")
	@Results({ @Result(column = "EMPID", property = "emplId"), @Result(column = "LASTNAME", property = "lname"),
			@Result(column = "FIRSTNAME", property = "fname"), @Result(column = "ADDRESS", property = "address"),
			@Result(column = "CITY", property = "city") })
	EmpVo findEmpById(int emplId);

}
