/**
 * 
 */
package com.delta.cru.rest.poc.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
public class EmplPhVo implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8643993196757108186L;
	
	/** The empl id. */
	private String emplId;
	
	/** The ph typ cd. */
	private String phTypCd;
	
	/** The empl ph nb. */
	private String emplPhNb;
	
	/** The eff dt. */
	private Date effDt;
	
	/** The exp dt. */
	private Date expDt;

	/**
	 * Gets the empl id.
	 *
	 * @return the empl id
	 */
	public String getEmplId() {
		return emplId;
	}

	/**
	 * Sets the empl id.
	 *
	 * @param emplId the new empl id
	 */
	public void setEmplId(String emplId) {
		this.emplId = emplId;
	}

	/**
	 * Gets the ph typ cd.
	 *
	 * @return the ph typ cd
	 */
	public String getPhTypCd() {
		return phTypCd;
	}

	/**
	 * Sets the ph typ cd.
	 *
	 * @param phTypCd the new ph typ cd
	 */
	public void setPhTypCd(String phTypCd) {
		this.phTypCd = phTypCd;
	}

	/**
	 * Instantiates a new empl ph vo.
	 */
	public EmplPhVo() {
		super();
	}

	/**
	 * Gets the empl ph nb.
	 *
	 * @return the empl ph nb
	 */
	public String getEmplPhNb() {
		return emplPhNb;
	}

	/**
	 * Sets the empl ph nb.
	 *
	 * @param emplPhNb the new empl ph nb
	 */
	public void setEmplPhNb(String emplPhNb) {
		this.emplPhNb = emplPhNb;
	}

	/**
	 * Gets the eff dt.
	 *
	 * @return the eff dt
	 */
	public Date getEffDt() {
		return effDt;
	}

	/**
	 * Sets the eff dt.
	 *
	 * @param effDt the new eff dt
	 */
	public void setEffDt(Date effDt) {
		this.effDt = effDt;
	}

	/**
	 * Gets the exp dt.
	 *
	 * @return the exp dt
	 */
	public Date getExpDt() {
		return expDt;
	}

	/**
	 * Sets the exp dt.
	 *
	 * @param expDt the new exp dt
	 */
	public void setExpDt(Date expDt) {
		this.expDt = expDt;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
