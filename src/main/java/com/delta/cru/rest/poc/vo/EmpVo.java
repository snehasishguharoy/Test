/**
 * 
 */
package com.delta.cru.rest.poc.vo;

import java.io.Serializable;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
public class EmpVo implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8642735862039852046L;

	/** The empl id. */
	private Integer emplId;

	/** The fname. */
	private String fname;

	/** The lname. */
	private String lname;

	/** The address. */
	private String address;

	/**
	 * Instantiates a new emp vo.
	 *
	 * @param emplId
	 *            the empl id
	 * @param fname
	 *            the fname
	 * @param lname
	 *            the lname
	 * @param address
	 *            the address
	 * @param city
	 *            the city
	 */
	public EmpVo(Integer emplId, String fname, String lname, String address, String city) {
		super();
		this.emplId = emplId;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.city = city;
	}

	/** The city. */
	private String city;

	/**
	 * Gets the empl id.
	 *
	 * @return the empl id
	 */
	public Integer getEmplId() {
		return emplId;
	}

	/**
	 * Sets the empl id.
	 *
	 * @param emplId
	 *            the new empl id
	 */
	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}

	/**
	 * Gets the fname.
	 *
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * Sets the fname.
	 *
	 * @param fname
	 *            the new fname
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * Gets the lname.
	 *
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * Sets the lname.
	 *
	 * @param lname
	 *            the new lname
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city
	 *            the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Instantiates a new emp vo.
	 */
	public EmpVo() {
		super();
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
