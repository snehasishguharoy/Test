/**
 * 
 */
package com.delta.cru.rest.poc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.delta.cru.rest.poc.mapper.EmpMapper;
import com.delta.cru.rest.poc.vo.EmpVo;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
@Repository
public class EmpDaoImpl implements EmpDaoIfc {
	
	/** The emp mapper. */
	@Autowired
	private EmpMapper empMapper;

	/* (non-Javadoc)
	 * @see com.delta.cru.rest.poc.dao.EmpDaoIfc#fEmpById()
	 */
	@Override
	public List<EmpVo> fEmpById() {
		return empMapper.findAllEmps();
	}

	/* (non-Javadoc)
	 * @see com.delta.cru.rest.poc.dao.EmpDaoIfc#findEmpById(int)
	 */
	@Override
	public EmpVo findEmpById(int empId) {
		return empMapper.findEmpById(empId);
	}


}
