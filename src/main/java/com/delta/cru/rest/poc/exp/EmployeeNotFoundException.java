/**
 * 
 */
package com.delta.cru.rest.poc.exp;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
public class EmployeeNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public EmployeeNotFoundException(String message) {
		super(message);
	}

}
