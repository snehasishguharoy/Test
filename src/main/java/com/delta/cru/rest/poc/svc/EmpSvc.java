/**
 * 
 */
package com.delta.cru.rest.poc.svc;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.cru.rest.poc.bo.EmpDtlsIfc;
import com.delta.cru.rest.poc.exp.EmployeeNotFoundException;
import com.delta.cru.rest.poc.vo.EmpVo;

/**
 *
 */
@RestController
public class EmpSvc {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(EmpSvc.class.getName());

	/** The dtls ifc. */
	@Autowired
	private EmpDtlsIfc dtlsIfc;

	/**
	 * Gets the all emp details.
	 *
	 * @return the all emp details
	 */
	@RequestMapping(value = "/names", method = RequestMethod.GET, produces = { "application/json" })
	public @ResponseBody List<EmpVo> getAllEmpDetails()  throws EmployeeNotFoundException{
		LOGGER.info("Printing the entire employee details");
		return dtlsIfc.findEmployeeById();
	}

	/**
	 * Find emp by id.
	 *
	 * @param empid
	 *            the empid
	 * @return the emp vo
	 */
	@RequestMapping(value = "/names/{empid}", method = RequestMethod.GET, produces = { "application/json" })
	public @ResponseBody EmpVo findEmpById(@PathVariable("empid") int empid)  throws EmployeeNotFoundException {
		LOGGER.info("Printing the particular employee detail");
		return dtlsIfc.findEmpById(empid);

	}

	
}
