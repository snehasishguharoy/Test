/**
 * 
 */
package com.delta.cru.rest.poc.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.delta.cru.rest.poc.vo.EmpVo;
import com.delta.cru.rest.poc.vo.EmplPhVo;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
@Configuration
@MappedTypes(value = { EmpVo.class, EmplPhVo.class })
@MapperScan("com.delta.cru.rest.poc.mapper")
public class EmpRstCnfg {
	
	/**
	 * The client id.
	 *
	 * @return the data source
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws NamingException
	 *             the naming exception
	 */

	@Bean
	public DataSource dataSource() throws IllegalArgumentException, NamingException {

		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
		bean.setJndiName("jdbc/oracle");
		bean.setResourceRef(true);
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		return (DataSource) bean.getObject();

	}

	/**
	 * Session factory.
	 *
	 * @return the sql session factory
	 * @throws Exception
	 *             the exception
	 */
	@Bean
	public SqlSessionFactory sessionFactory() throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource());
		return bean.getObject();
	}


}
